package net.wykxrdlm.jftgv;

import java.net.URL;
import java.util.HashMap;
import java.util.ResourceBundle;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.ContextMenuEvent;

public class InterfaceController implements Initializable
{
	
	//announcement of all objects with which we will work 
	InterfaceHandler handler;
	
	@FXML
    private TextField input_degrees;
	@FXML
    private TextField input_iterations;
	@FXML
    private TextField input_precision;
	
	@FXML
	private RadioButton radio_sin;
	@FXML
	private RadioButton radio_cos;
	@FXML
	private RadioButton radio_tg;
	@FXML
	private RadioButton radio_ctg;
	@FXML
	private RadioButton radio_sec;
	@FXML
	private RadioButton radio_cosec;
	
	@FXML
	private Button button_save;
	@FXML
	private Button button_copy;
	
	@FXML
	private Button button_start;
	
	@FXML
	private Label label_version;
	@FXML
	private Label label_output;
	@FXML
	private Label label_process;
	@FXML
	private Label label_title;
	@FXML
	private Label label_creator;
	@FXML
	private Label label_translate;
	
	@FXML
	private Label label_degrees;
	@FXML
	private Label label_iterations;
	@FXML
	private Label label_precision;
	@FXML
	private Label label_function;
	@FXML
	private Label label_start;
	
	@FXML
	private TextArea text_output;
		
	//init
	@Override
    public void initialize(URL location, ResourceBundle resources) 
	{
		
		handler = new InterfaceHandler(this);

		//limiting the spelling of "bad" characters 
		input_degrees = InputListeners.addDegreesListener(input_degrees);
		input_iterations = InputListeners.addIterationsListener(input_iterations);
		input_precision = InputListeners.addPrecisionListener(input_precision);
		
		//JavaFX has a function for opening context windows. I do not like this
		input_degrees.addEventFilter(ContextMenuEvent.CONTEXT_MENU_REQUESTED, Event::consume);
		input_iterations.addEventFilter(ContextMenuEvent.CONTEXT_MENU_REQUESTED, Event::consume);
		input_precision.addEventFilter(ContextMenuEvent.CONTEXT_MENU_REQUESTED, Event::consume);
		text_output.addEventFilter(ContextMenuEvent.CONTEXT_MENU_REQUESTED, Event::consume);
		
		//bringing the interface to its original form
		setTextLabelTitle("jFTGV");
		setBoolRadioSin(true);
		setTextLabelProcess(0, 0);
		setButtonStartEnable(true);
		setButtonSaveEnable(false);
		setButtonCopyEnable(false);
		setAllTranslateLines();
		
    }
	
	//we translate all interface elements with Language class
	private void setAllTranslateLines()
	{
		HashMap<String, String> lines = Language.getTranslateLines();
		
		label_version.setText( lines.get("version") + ProgramConstants.programVersion );
		label_creator.setText( lines.get("creator") + ProgramConstants.programCreator );
		label_translate.setText( lines.get("translate") );
		
		label_degrees.setText( lines.get("degrees") );
		label_iterations.setText( lines.get("iterations") );
		label_precision.setText( lines.get("precision") );
		label_function.setText( lines.get("function") );
		label_start.setText( lines.get("start") );
		label_output.setText( lines.get("output") );
		
		button_start.setText( lines.get("b_start") );
		button_copy.setText( lines.get("b_copy") );
		button_save.setText( lines.get("b_save") );
	}

	//listener functions
	@FXML
    public void onInputDegreesAction() //that don't works!
	{
		
    }
	
	@FXML
    public void onInputIterationsAction() //that don't works!
	{
		
    }
	
	@FXML
    public void onInputPrecisionAction() //that don't works!
	{
		
    }
	
	@FXML
    public void onRadioSinAction()
	{
		RadioSwitcher.enableRadioSin(this);
    }
	
	@FXML
    public void onRadioCosAction()
	{
		RadioSwitcher.enableRadioCos(this);
    }
	
	@FXML
    public void onRadioTgAction()
	{
		RadioSwitcher.enableRadioTg(this);
    }
	
	@FXML
    public void onRadioCtgAction()
	{
		RadioSwitcher.enableRadioCtg(this);
    }
	
	@FXML
    public void onRadioSecAction()
	{
		RadioSwitcher.enableRadioSec(this);
    }
	
	@FXML
    public void onRadioCosecAction()
	{
		RadioSwitcher.enableRadioCosec(this);
    }
	
	//listener for the start button. important!
	@FXML
    public void onButtonStartClick()
	{
		//collects into an array all the values of the radio buttons to select a function. why not?
		boolean[] funcSelectedArray = 
			{
				radio_sin.isSelected(), 
				radio_cos.isSelected(), 
				radio_tg.isSelected(), 
				radio_ctg.isSelected(),
				radio_sec.isSelected(),
				radio_cosec.isSelected()
			};		
		//runs a function in the handler to start
		handler.runStart(input_degrees.getText(), input_iterations.getText(), input_precision.getText(), funcSelectedArray);
    }
	
	@FXML
	public void onButtonSaveClick()
	{
		handler.runSaveOutputText();
	}
	
	@FXML
	public void onButtonCopyClick()
	{
		handler.runCopyOutputText();
	}
	
	
	//setters and getters
	public void setTextInputDegrees(String text)
	{
		input_degrees.setText(text);
	}
	
	public void setTextInputIterations(String text)
	{
		input_iterations.setText(text);
	}
	
	public void setTextInputPrecision(String text)
	{
		input_precision.setText(text);
	}
	
	public void setBoolRadioSin(boolean bool)
	{
		radio_sin.setSelected(bool);
	}
	
	public void setBoolRadioCos(boolean bool)
	{
		radio_cos.setSelected(bool);
	}
	
	public void setBoolRadioTg(boolean bool)
	{
		radio_tg.setSelected(bool);
	}
	
	public void setBoolRadioCtg(boolean bool)
	{
		radio_ctg.setSelected(bool);
	}
	
	public void setBoolRadioSec(boolean bool)
	{
		radio_sec.setSelected(bool);
	}
	
	public void setBoolRadioCosec(boolean bool)
	{
		radio_cosec.setSelected(bool);
	}
	
	public void setTextOutput(String text)
	{
		text_output.setText(text);
	}
	
	public void setButtonStartEnable(boolean bool)
	{
		button_start.setDisable(!bool);
	}
	
	public void setButtonSaveEnable(boolean bool)
	{
		button_save.setDisable(!bool);
	}
	
	public void setButtonCopyEnable(boolean bool)
	{
		button_copy.setDisable(!bool);
	}
	
	public void setTextLabelVersion(String version)
	{
		label_version.setText(version);
	}
	
	public void setTextLabelOutput(String text)
	{
		label_output.setText(text);
	}
	
	public void setTextLabelProcess(int process, int end)
	{
		label_process.setText(String.valueOf(process) + "/" + String.valueOf(end));
	}
	
	public void setTextLabelTitle(String text)
	{
		label_title.setText(text);
	}
	
	public void setTextLabelCreator(String text)
	{
		label_creator.setText(text);
	}
	
	public void setTextLabelTranslate(String text)
	{
		label_translate.setText(text);
	}
	
	public void setTextLabelDegrees(String text)
	{
		label_degrees.setText(text);
	}
	
	public void setTextLabelIterations(String text)
	{
		label_iterations.setText(text);
	}
	
	public void setTextLabelFunction(String text)
	{
		label_function.setText(text);
	}
	
	public void setTextLabelStart(String text)
	{
		label_start.setText(text);
	}
	
	public String getInputDegrees()
	{
		return input_degrees.getText();
	}
	
	public String getInputIterations()
	{
		return input_iterations.getText();
	}
	
	public String getInputPrecision()
	{
		return input_precision.getText();
	}
	
	public String getOutputText()
	{
		return text_output.getText();
	}
}
