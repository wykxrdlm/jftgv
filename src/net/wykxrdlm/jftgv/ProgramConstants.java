package net.wykxrdlm.jftgv;


//all constants for program
public class ProgramConstants
{
	//info constants
	public static final String programVersion = "02";
	public static final String programCreator = "wykxrdlm";
	
	//constants for global path
	public static final String propertiesFileExtension = ".conf";
	public static final String themesFileExtension = ".fxml";
	
	public static final String pathTranslates = "languages";
	public static final String pathThemes = "themes";
	
	public static final String propertiesFile = "/config" + propertiesFileExtension;
	
	//constants for local path
	public static final String defaultResourcesFolder = "/resources";
	public static final String defaultSettingsFile = "/default_config" + propertiesFileExtension;
	public static final String defaultLanguageFile = "/default_language" + propertiesFileExtension;
	public static final String defaultThemeFile = "/default_theme" + themesFileExtension;

}
