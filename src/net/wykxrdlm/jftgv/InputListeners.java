package net.wykxrdlm.jftgv;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TextField;

//add listeners to degrees input, iterations input and precision input
public class InputListeners
{
	
	public static TextField addDegreesListener (TextField textField)
	{
		textField.textProperty().addListener(new ChangeListener<String>() 
		{
		    @Override
		    public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) 
		    {
		    	//regular expression for floating point number (8 digits before the decimal point and 6 digits after the decimal point)
		    	//also can have any sign
		    	if (!newValue.matches("-?\\d{0,8}([\\.]\\d{0,6})?")) 
		    	{
		    		textField.setText(oldValue);
			    }
		    }
		});

		return textField;
	}
	
	public static TextField addIterationsListener (TextField textField)
	{
		textField.textProperty().addListener(new ChangeListener<String>() 
		{
			
		    @Override
		    public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) 
		    {
		    	//regular expression just for integer
		        if (!newValue.matches("\\d{0,18}")) 
		        {
		        	textField.setText(oldValue);
		        }
		    }
		});
		
		return textField;
	}
	
	public static TextField addPrecisionListener (TextField textField)
	{
		textField.textProperty().addListener(new ChangeListener<String>() 
		{
			
		    @Override
		    public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) 
		    {
		    	//regular expression just for integer
		        if (!newValue.matches("\\d{0,18}")) 
		        {
		        	textField.setText(oldValue);
		        }
		    }
		});
		
		return textField;
	}
}
