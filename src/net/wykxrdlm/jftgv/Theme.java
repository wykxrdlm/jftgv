package net.wykxrdlm.jftgv;

import java.io.File;
import java.net.URL;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;

//load fxml file (theme)
public class Theme
{
	
	public static Parent loadTheme(String themeFileName)
	{
		URL themeUrl;
		FXMLLoader loader = new FXMLLoader();
		Parent root = null;
		
		try
		{
			
			String dir = System.getProperty("user.dir");
			themeUrl = new File(dir + File.separator + ProgramConstants.pathThemes + File.separator + themeFileName + File.separator + "theme" +  ProgramConstants.themesFileExtension).toURI().toURL();
			loader.setLocation(themeUrl);
			root = loader.load();
			
			System.out.println("External theme file loaded successfully!");
		} 
		//catch if external fxml file was loaded with errors
		catch (Exception e)
		{
			System.out.println("An error occurred while loading the external theme file! The default theme file is loaded.");
			
			try
			{
				themeUrl = Theme.class.getResource(ProgramConstants.defaultResourcesFolder + ProgramConstants.defaultThemeFile);
				loader.setLocation(themeUrl);
				System.out.println("Default theme file loaded successfully!");
				root = loader.load();
			}
			//catch if DEFAULT fxml file was loaded with errors
			//means a serious error somewhere
			catch (Exception e2)
			{
				e2.printStackTrace();
				System.out.println("An error occurred while loading the theme file!");
			}
		}
		
		return root;
	}

}
