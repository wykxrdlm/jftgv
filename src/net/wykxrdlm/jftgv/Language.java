package net.wykxrdlm.jftgv;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Properties;

//load configuration language file class
public class Language
{	
	private static HashMap<String, String> translateLines = new HashMap<String, String>();
	
	public static void loadLanguage(String translateFileName)
	{
		Properties prop = new Properties();
		try
		{
			
			String dir = System.getProperty("user.dir");
			InputStream externalStream = new FileInputStream(dir + File.separator + ProgramConstants.pathTranslates + File.separator + translateFileName + ProgramConstants.propertiesFileExtension);
			prop.load(externalStream);
			externalStream.close();
			setAllLines(prop);
			
			System.out.println("External translate file loaded successfully!");
		} 
		//catch if external language file was loaded with errors
		catch (Exception e)
		{
			System.out.println("An error occurred while loading the external translate file! The default translate file is loaded.");
			InputStream defaultStream = Language.class.getResourceAsStream(ProgramConstants.defaultResourcesFolder + ProgramConstants.defaultLanguageFile);
			
			try
			{
				prop.load(defaultStream);
				defaultStream.close();
				setAllLines(prop);
				System.out.println("Default translate file loaded successfully!");
			}
			//catch if DEFAULT language file was loaded with errors
			//means a serious error somewhere
			catch (Exception e2)
			{
				//e2.printStackTrace(); maybe later
				System.out.println("An error occurred while loading the default translate file!");
			}
		}
	}
	
	//generate new HashMap and catch errors
	private static void setAllLines(Properties p)
	{
		translateLines.put("creator", p.getProperty("info_creator_text"));
		translateLines.put("version", p.getProperty("info_version_text"));
		translateLines.put("translate", p.getProperty("info_translate_text"));
		translateLines.put("wait", p.getProperty("info_wait"));
		translateLines.put("notcalc", p.getProperty("info_not_calc"));
		translateLines.put("degrees", p.getProperty("label_degrees_text"));
		translateLines.put("iterations", p.getProperty("label_iterations_text"));
		translateLines.put("precision", p.getProperty("label_precision_text"));
		translateLines.put("function", p.getProperty("label_function_text"));
		translateLines.put("start", p.getProperty("label_start_text"));
		translateLines.put("output", p.getProperty("label_output_text"));
		translateLines.put("b_start", p.getProperty("button_start_text"));
		translateLines.put("b_copy", p.getProperty("button_copy_text"));
		translateLines.put("b_save", p.getProperty("button_save_text"));
	}
	
	//log all translated lines
	public static void logTranslate()
	{
		System.out.println(translateLines.toString());
		
	}
	
	//getter
	public static HashMap<String, String> getTranslateLines()
	{
		return translateLines;
	}
	
}
