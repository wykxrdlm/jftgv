package net.wykxrdlm.jftgv;

import java.math.BigDecimal;
import java.math.RoundingMode;

//reduces all input values to uniformity. magic code! 
public class InputFix
{
	public static void fixDegrees(String textDegrees, InterfaceController controller)
	{
		if (textDegrees.equals(""))
		{
			controller.setTextInputDegrees("0");
		}
		
		else
		{
			if (textDegrees.toCharArray()[0] == '.')
			{
				textDegrees = "0" + textDegrees;
			}
		
			double newDegrees = Double.parseDouble(textDegrees);
			BigDecimal bigDegrees = new BigDecimal(newDegrees % 360.0).setScale(6, RoundingMode.HALF_DOWN);
			controller.setTextInputDegrees(String.valueOf(bigDegrees));
		}
	}
	
	public static void fixIterations(String textIter, InterfaceController controller)
	{
		
		
		if (textIter.equals(""))
		{
			controller.setTextInputIterations(String.valueOf(Settings.getMinIterationsCount()));
		}
		
		else
		{
		
			long intIter = Long.parseLong(textIter);
			if (intIter < 1 || intIter < Settings.getMinIterationsCount())
			{
				controller.setTextInputIterations(String.valueOf(Settings.getMinIterationsCount()));
			}
			
			else if (intIter > Settings.getMaxIterationsCount())
			{
				controller.setTextInputIterations(String.valueOf(Settings.getMaxIterationsCount()));
			}
			
			else
			{
				controller.setTextInputIterations(String.valueOf(intIter));
			}
		}
	}
	
	public static void fixPrecision(String textPrec, InterfaceController controller)
	{
		if (textPrec.equals(""))
		{
			controller.setTextInputPrecision(String.valueOf(Settings.getMinPrecisionCount()));
		}
		else
		{
		
			long intPrec = Long.parseLong(textPrec);
			if (intPrec < 1 || intPrec < Settings.getMinPrecisionCount())
			{
				controller.setTextInputPrecision(String.valueOf(Settings.getMinPrecisionCount()));
			}
			
			else if (intPrec > Settings.getMaxPrecisionCount())
			{
				controller.setTextInputPrecision(String.valueOf(Settings.getMaxPrecisionCount()));
			}
			
			else
			{
				controller.setTextInputPrecision(String.valueOf(intPrec));
			}
		}
	}
}
