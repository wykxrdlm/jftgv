package net.wykxrdlm.jftgv;

import java.math.*;

import javafx.application.Platform;

//calculates trigonometric functions
//uses taylor series
//overloaded functions so that the jar file can be used in other programs 
//optimized by removing power and factorial functions 

public class CalcTrigonometricValue
{	
	private static final RoundingMode rM = RoundingMode.HALF_UP;
	
	public static String calcSin(String degrees, int iter, int prec, boolean force)
	{

		int realPrec = prec + 8;
		MathContext mc = new MathContext(realPrec, rM);
		BigDecimal bigDegrees = new BigDecimal(degrees);
		final BigDecimal piDegrees = ((bigDegrees.divide(new BigDecimal(180), mc)).multiply(PiValue.PI)).setScale(realPrec, rM);
		
		BigDecimal piWorkDegrees = piDegrees;
		
		String lastValue = "";
		
		int st = 3;
		
		BigDecimal ret = BigDecimal.ONE;
		ret = ret.multiply(new BigDecimal("2"));
		ret = ret.multiply(new BigDecimal("3"));
		
		BigDecimal powPiDegrees = piDegrees.pow(st);
		
		for(int i = 0; i < iter; i++)
		{		
			piWorkDegrees = (piWorkDegrees.add(((powPiDegrees).divide(ret, mc))
					.multiply(new BigDecimal(-1)))).setScale(realPrec, rM);
			st = st + 2;
			
			powPiDegrees = (powPiDegrees.multiply(piDegrees)).multiply(piDegrees).setScale(realPrec, rM);
			
			ret = (ret.multiply(new BigDecimal( String.valueOf(st-1) ))).setScale(realPrec, rM);
			ret = (ret.multiply(new BigDecimal( String.valueOf(st) ))).setScale(realPrec, rM);
			
			piWorkDegrees = (piWorkDegrees.add(((powPiDegrees).divide(ret, mc))
					.multiply(new BigDecimal(1)))).setScale(realPrec, rM);
			st = st + 2;
			
			powPiDegrees = (powPiDegrees.multiply(piDegrees)).multiply(piDegrees).setScale(realPrec, rM);
			
			ret = (ret.multiply(new BigDecimal( String.valueOf(st-1) ))).setScale(realPrec, rM);
			ret = (ret.multiply(new BigDecimal( String.valueOf(st) ))).setScale(realPrec, rM);
			
			System.out.println("piWorkDegrees: " + piWorkDegrees.toPlainString());
			System.out.println("This is " + String.valueOf(i) +" out of " + String.valueOf(iter) +" iterations in calculating the sin");
			
			if (lastValue.equals(piWorkDegrees.toPlainString()) && !force)
			{
				break;
			}
			
			lastValue = piWorkDegrees.toPlainString();
			
			
		}
		
		piWorkDegrees = piWorkDegrees.setScale(prec, rM);
	
		return piWorkDegrees.toPlainString(); 
		
	}
	
	public static String calcCos(String degrees, int iter, int prec, boolean force)
	{
		
		
		
		int realPrec = prec + 8;
		MathContext mc = new MathContext(realPrec, rM) ;
		BigDecimal bigDegrees = new BigDecimal(degrees);
		final BigDecimal piDegrees = ((bigDegrees.divide(new BigDecimal(180), mc)).multiply(PiValue.PI)).setScale(realPrec, rM);
		
		BigDecimal piWorkDegrees = new BigDecimal(1);
		
		String lastValue = "";
		
		int st = 2;
		
		BigDecimal ret = BigDecimal.ONE;
		ret = ret.multiply(new BigDecimal("2"));
		
		BigDecimal powPiDegrees = piDegrees.pow(st);
		
		for(int i = 0; i < iter; i++)
		{
			
			piWorkDegrees = (piWorkDegrees.add(((powPiDegrees).divide(ret, mc))
					.multiply(new BigDecimal(-1)))).setScale(realPrec, rM);
			st = st + 2;
			
			powPiDegrees = (powPiDegrees.multiply(piDegrees)).multiply(piDegrees).setScale(realPrec, rM);
			
			ret = (ret.multiply(new BigDecimal( String.valueOf(st-1) ))).setScale(realPrec, rM);
			ret = (ret.multiply(new BigDecimal( String.valueOf(st) ))).setScale(realPrec, rM);
			
			piWorkDegrees = (piWorkDegrees.add(((powPiDegrees).divide(ret, mc))
					.multiply(new BigDecimal(1)))).setScale(realPrec, rM);
			st = st + 2;
			
			powPiDegrees = (powPiDegrees.multiply(piDegrees)).multiply(piDegrees).setScale(realPrec, rM);
			
			ret = (ret.multiply(new BigDecimal( String.valueOf(st-1) ))).setScale(realPrec, rM);
			ret = (ret.multiply(new BigDecimal( String.valueOf(st) ))).setScale(realPrec, rM);
			
			System.out.println("piWorkDegrees: " + piWorkDegrees.toPlainString());
			System.out.println("This is " + String.valueOf(i) +" out of " + String.valueOf(iter) +" iterations in calculating the cos");
			
			if (lastValue.equals(piWorkDegrees.toPlainString()) && !force)
			{
				break;
			}
			
			lastValue = piWorkDegrees.toPlainString();
			
			
		}
		
		piWorkDegrees = piWorkDegrees.setScale(prec, rM);
	
		return piWorkDegrees.toPlainString(); 
		
	}

	
	public static String calcTg(String degrees, int iter, int prec, boolean force)
	{
		String sinResult = calcSin(degrees, iter, prec+8, force);
		String cosResult = calcCos(degrees, iter, prec+8, force);
		
		BigDecimal workSinResult = new BigDecimal(sinResult);
		BigDecimal workCosResult = new BigDecimal(cosResult);
		
		BigDecimal out = (workSinResult.divide(workCosResult, prec+8, rM)).setScale(prec, rM);
		
		return out.toPlainString();
		
	}
	
	public static String calcCtg(String degrees, int iter, int prec, boolean force)
	{
		String sinResult = calcSin(degrees, iter, prec+8, force);
		String cosResult = calcCos(degrees, iter, prec+8, force);
		
		BigDecimal workSinResult = new BigDecimal(sinResult);
		BigDecimal workCosResult = new BigDecimal(cosResult);
		
		BigDecimal out = (workCosResult.divide(workSinResult, prec+8, rM)).setScale(prec, rM);
		
		return out.toPlainString();
		
	}
	
	public static String calcSec(String degrees, int iter, int prec, boolean force)
	{
		String cosResult = calcCos(degrees, iter, prec+8, force);
		BigDecimal workCosResult = new BigDecimal(cosResult);
		
		BigDecimal out = (BigDecimal.ONE.divide(workCosResult, prec+8, rM)).setScale(prec, rM);
		
		return out.toPlainString();
		
	}
	
	public static String calcCosec(String degrees, int iter, int prec, boolean force)
	{
		String sinResult = calcSin(degrees, iter, prec+8, force);
		BigDecimal workSinResult = new BigDecimal(sinResult);
		
		BigDecimal out = (BigDecimal.ONE.divide(workSinResult, prec+8, rM)).setScale(prec, rM);
		
		return out.toPlainString();
		
	}
	
	public static String calcSin(String degrees, int iter, int prec, boolean force, InterfaceController controller)
	{

		int realPrec = prec + 8;
		MathContext mc = new MathContext(realPrec, rM);
		BigDecimal bigDegrees = new BigDecimal(degrees);
		final BigDecimal piDegrees = ((bigDegrees.divide(new BigDecimal(180), mc)).multiply(PiValue.PI)).setScale(realPrec, rM);
		
		BigDecimal piWorkDegrees = piDegrees;
		
		String lastValue = "";
		
		int st = 3;
		
		BigDecimal ret = BigDecimal.ONE;
		ret = ret.multiply(new BigDecimal("2"));
		ret = ret.multiply(new BigDecimal("3"));
		
		BigDecimal powPiDegrees = piDegrees.pow(st);
		
		for(int i = 0; i < iter; i++)
		{		
			piWorkDegrees = (piWorkDegrees.add(((powPiDegrees).divide(ret, mc))
					.multiply(new BigDecimal(-1)))).setScale(realPrec, rM);
			st = st + 2;
			
			powPiDegrees = (powPiDegrees.multiply(piDegrees)).multiply(piDegrees).setScale(realPrec, rM);
			
			ret = (ret.multiply(new BigDecimal( String.valueOf(st-1) ))).setScale(realPrec, rM);
			ret = (ret.multiply(new BigDecimal( String.valueOf(st) ))).setScale(realPrec, rM);
			
			piWorkDegrees = (piWorkDegrees.add(((powPiDegrees).divide(ret, mc))
					.multiply(new BigDecimal(1)))).setScale(realPrec, rM);
			st = st + 2;
			
			powPiDegrees = (powPiDegrees.multiply(piDegrees)).multiply(piDegrees).setScale(realPrec, rM);
			
			ret = (ret.multiply(new BigDecimal( String.valueOf(st-1) ))).setScale(realPrec, rM);
			ret = (ret.multiply(new BigDecimal( String.valueOf(st) ))).setScale(realPrec, rM);
			
			int count = i+1;
			Platform.runLater(() -> {
				controller.setTextLabelProcess(count, iter);
			});
			
			System.out.println("piWorkDegrees: " + piWorkDegrees.toPlainString());
			System.out.println("This is " + String.valueOf(i) +" out of " + String.valueOf(iter) +" iterations in calculating the sin");
			
			if (lastValue.equals(piWorkDegrees.toPlainString()) && !force)
			{
				break;
			}
			
			lastValue = piWorkDegrees.toPlainString();
		}
		
		piWorkDegrees = piWorkDegrees.setScale(prec, rM);
	
		return piWorkDegrees.toPlainString(); 
		
	}
	
	public static String calcCos(String degrees, int iter, int prec, boolean force, InterfaceController controller)
	{
		
		
		
		int realPrec = prec + 8;
		MathContext mc = new MathContext(realPrec, rM) ;
		BigDecimal bigDegrees = new BigDecimal(degrees);
		final BigDecimal piDegrees = ((bigDegrees.divide(new BigDecimal(180), mc)).multiply(PiValue.PI)).setScale(realPrec, rM);
		
		BigDecimal piWorkDegrees = new BigDecimal(1);
		
		String lastValue = "";
		
		int st = 2;
		
		BigDecimal ret = BigDecimal.ONE;
		ret = ret.multiply(new BigDecimal("2"));
		
		BigDecimal powPiDegrees = piDegrees.pow(st);
		
		for(int i = 0; i < iter; i++)
		{
			
			piWorkDegrees = (piWorkDegrees.add(((powPiDegrees).divide(ret, mc))
					.multiply(new BigDecimal(-1)))).setScale(realPrec, rM);
			st = st + 2;
			
			powPiDegrees = (powPiDegrees.multiply(piDegrees)).multiply(piDegrees).setScale(realPrec, rM);
			
			ret = (ret.multiply(new BigDecimal( String.valueOf(st-1) ))).setScale(realPrec, rM);
			ret = (ret.multiply(new BigDecimal( String.valueOf(st) ))).setScale(realPrec, rM);
			
			piWorkDegrees = (piWorkDegrees.add(((powPiDegrees).divide(ret, mc))
					.multiply(new BigDecimal(1)))).setScale(realPrec, rM);
			st = st + 2;
			
			powPiDegrees = (powPiDegrees.multiply(piDegrees)).multiply(piDegrees).setScale(realPrec, rM);
			
			ret = (ret.multiply(new BigDecimal( String.valueOf(st-1) ))).setScale(realPrec, rM);
			ret = (ret.multiply(new BigDecimal( String.valueOf(st) ))).setScale(realPrec, rM);
			
			
			int count = i+1;
			Platform.runLater(() -> {
				controller.setTextLabelProcess(count, iter);
			});
			
			System.out.println("piWorkDegrees: " + piWorkDegrees.toPlainString());
			System.out.println("This is " + String.valueOf(i) +" out of " + String.valueOf(iter) +" iterations in calculating the cos");
			
			if (lastValue.equals(piWorkDegrees.toPlainString()) && !force)
			{
				break;
			}
			
			lastValue = piWorkDegrees.toPlainString();
			
		}
		
		
		
		piWorkDegrees = piWorkDegrees.setScale(prec, rM);
	
		return piWorkDegrees.toPlainString(); 
		
	}

	
	public static String calcTg(String degrees, int iter, int prec, boolean force, InterfaceController controller)
	{
		String sinResult = calcSin(degrees, iter, prec+8, force, controller);
		String cosResult = calcCos(degrees, iter, prec+8, force, controller);
		
		BigDecimal workSinResult = new BigDecimal(sinResult);
		BigDecimal workCosResult = new BigDecimal(cosResult);
		
		BigDecimal out = (workSinResult.divide(workCosResult, prec+8, rM)).setScale(prec, rM);
		
		return out.toPlainString();
		
	}
	
	public static String calcCtg(String degrees, int iter, int prec, boolean force, InterfaceController controller)
	{
		String sinResult = calcSin(degrees, iter, prec+8, force, controller);
		String cosResult = calcCos(degrees, iter, prec+8, force, controller);
		
		BigDecimal workSinResult = new BigDecimal(sinResult);
		BigDecimal workCosResult = new BigDecimal(cosResult);
		
		BigDecimal out = (workCosResult.divide(workSinResult, prec+8, rM)).setScale(prec, rM);
		
		return out.toPlainString();
		
	}
	
	public static String calcSec(String degrees, int iter, int prec, boolean force, InterfaceController controller)
	{
		String cosResult = calcCos(degrees, iter, prec+8, force, controller);
		BigDecimal workCosResult = new BigDecimal(cosResult);
		
		BigDecimal out = (BigDecimal.ONE.divide(workCosResult, prec+8, rM)).setScale(prec, rM);
		
		return out.toPlainString();
		
	}
	
	public static String calcCosec(String degrees, int iter, int prec, boolean force, InterfaceController controller)
	{
		String sinResult = calcSin(degrees, iter, prec+8, force, controller);
		BigDecimal workSinResult = new BigDecimal(sinResult);
		
		BigDecimal out = (BigDecimal.ONE.divide(workSinResult, prec+8, rM)).setScale(prec, rM);
		
		return out.toPlainString();
		
	}
	

	
	
}
