
/************************************************/
/* jFTGV - Find Trigonometric Value on Java     */
/* This program calculates basic trigonometric  */
/* functions with very high precision. This     */
/* program also supports                        */
/* custom themes, language extensions           */
/* and settings. Only Java 1.8.0!               */
/*                                              */
/* Made by wykxrdlm (Nikita Shevchenko).        */
/************************************************/

package net.wykxrdlm.jftgv;

import java.io.IOException;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

//main class for starting the graphics window and run program
public class FindTrigonometricValue extends Application
{
	
	 public static void main(String[] args) 
	 {
		 launch(args);
	 }
	 
	 @Override
	 public void start(Stage primaryStage) throws IOException 
	 {
		 Settings.loadSettings();
		 Settings.logSettings();
		 Language.loadLanguage(Settings.getTranslateFile());
		 
		 Parent root = Theme.loadTheme(Settings.getThemeFile());
		 
		 primaryStage.setScene(new Scene(root, Settings.getAppWidth(), Settings.getAppHeight()));
		 
		 //to output to the console when the program is closed
		 primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() 
		 {
			 @Override
			 public void handle(WindowEvent t) 
			 {
				 System.out.println("The program was closed");
				 Platform.exit();
				 System.exit(0);
			 }
		 } );
		 
		
		 primaryStage.setResizable(Settings.getAppResizeable());
		 primaryStage.setTitle("jFTGV");
		 primaryStage.show();
		 
		 System.out.println(System.getProperty("user.dir"));

	 }


}
