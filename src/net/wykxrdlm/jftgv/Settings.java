package net.wykxrdlm.jftgv;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

//load main configuration file with all settings like theme or language file name or logic variables
public class Settings
{	
	private static String translateFile;
	private static String themeFile;
	
	private static int appWidth;
	private static int appHeight;
	private static boolean appResizeable;
	
	private static boolean forceIterations;
	
	private static int minIterationsCount;
	private static int maxIterationsCount;
	
	private static int minPrecisionCount;
	private static int maxPrecisionCount;
	
	public static void loadSettings()
	{
		Properties prop = new Properties();
		try
		{
			
			String dir = System.getProperty("user.dir");
			InputStream externalStream = new FileInputStream(dir + ProgramConstants.propertiesFile);
			prop.load(externalStream);
			externalStream.close();
			Settings.setAllSettings(prop);
			
			System.out.println("External config file loaded successfully!");
		} 
		//catch if external config file was loaded with errors or not existed
		catch (Exception e)
		{
			System.out.println("An error occurred while loading the external config file! The default config file is loaded.");
			InputStream defaultStream = Settings.class.getResourceAsStream(ProgramConstants.defaultResourcesFolder + ProgramConstants.defaultSettingsFile);
			
			try
			{
				prop.load(defaultStream);
				defaultStream.close();
				Settings.setAllSettings(prop);
				System.out.println("Default config file loaded successfully!");
			}
			//catch if DEFAULT config file was loaded with errors
			//means a serious error somewhere
			catch (Exception e2)
			{
				e2.printStackTrace();
				System.out.println("An error occurred while loading the default config file!");
			}
		}
	}
	
	//set all vars and catch errors
	private static void setAllSettings(Properties p)
	{
		translateFile = p.getProperty("file_translate");
		themeFile = p.getProperty("file_theme");
		
		appWidth = Integer.parseInt(p.getProperty("app_width"));
		appHeight = Integer.parseInt(p.getProperty("app_height"));
		appResizeable = Boolean.parseBoolean(p.getProperty("app_resizeable"));
		
		
		forceIterations = Boolean.parseBoolean(p.getProperty("logic_force_iterations"));
		
		minIterationsCount = Integer.parseInt(p.getProperty("limits_iterations_min"));
		maxIterationsCount = Integer.parseInt(p.getProperty("limits_iterations_max"));
		
		if(minIterationsCount < 1)
		{
			minIterationsCount = 1;
		}
		
		minPrecisionCount = Integer.parseInt(p.getProperty("limits_precision_min"));
		maxPrecisionCount = Integer.parseInt(p.getProperty("limits_precision_max"));
		
		if(minPrecisionCount < 1)
		{
			minPrecisionCount = 1;
		}
	}
	
	//log all LOADED settings
	public static void logSettings()
	{
		System.out.println("All loaded settings:");
		System.out.println("\ttranslateFile=" + translateFile);
		System.out.println("\tthemeFile=" + themeFile);
		System.out.println("\tappWidth=" + String.valueOf(appWidth));
		System.out.println("\tappHeight=" + String.valueOf(appHeight));
		System.out.println("\tappResizeable=" + String.valueOf(appResizeable));
		System.out.println("\tforceIterations=" + String.valueOf(forceIterations));
		System.out.println("\tmaxIterationsCount=" + String.valueOf(maxIterationsCount));
		System.out.println("\tminIterationsCount=" + String.valueOf(minIterationsCount));
		System.out.println("\tmaxPrecisionCount=" + String.valueOf(maxPrecisionCount));
		System.out.println("\tminPrecisionCount=" + String.valueOf(minPrecisionCount));
		
	}
	
	//getters
	public static String getTranslateFile()
	{
		return translateFile;
	}
	
	public static String getThemeFile()
	{
		return themeFile;
	}
	
	public static int getAppWidth()
	{
		return appWidth;
	}
	
	public static int getAppHeight()
	{
		return appHeight;
	}
	
	public static boolean getForceIterations()
	{
		return forceIterations;
	}
	
	public static int getMinIterationsCount()
	{
		return minIterationsCount;
	}
	
	public static int getMaxIterationsCount()
	{
		return maxIterationsCount;
	}
	
	public static int getMinPrecisionCount()
	{
		return minPrecisionCount;
	}
	
	public static int getMaxPrecisionCount()
	{
		return maxPrecisionCount;
	}
	
	public static boolean getAppResizeable()
	{
		return appResizeable;
	}
}
