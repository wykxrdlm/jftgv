package net.wykxrdlm.jftgv;

import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.io.FileWriter;
import java.awt.Toolkit;
import java.math.BigDecimal;

import javax.swing.JFileChooser;

public class InterfaceHandler 
{
	InterfaceController controller;
	
	public InterfaceHandler(InterfaceController controller)
	{
		this.controller = controller;
	}
	
	public void runCopyOutputText()
	{
		StringSelection copyText = new StringSelection(controller.getOutputText());
		Clipboard cb = Toolkit.getDefaultToolkit().getSystemClipboard();
		cb.setContents(copyText, null);
		
		System.out.println("Output text copied to clipboard");
	}
	
	public void runSaveOutputText()
	{
		String out = controller.getOutputText();
	    JFileChooser chooser = new JFileChooser();
	    int retrival = chooser.showSaveDialog(null);
	    if (retrival == JFileChooser.APPROVE_OPTION) 
	    {
	        try 
	        {
	            FileWriter fw = new FileWriter(chooser.getSelectedFile());
	            fw.write(out.toString());
	            fw.close();
	        } 
	        catch (Exception ex) 
	        {
	            ex.printStackTrace();
	        }
	    }
	}
	
	public void runStart(String textDegrees, String textIter, String textPrec, boolean[] fArr)
	{
		controller.setButtonStartEnable(false);
		controller.setButtonSaveEnable(false);
		controller.setButtonCopyEnable(false);
		
		boolean sin = fArr[0];
		boolean cos = fArr[1];
		boolean tg = fArr[2];
		boolean ctg = fArr[3];
		boolean sec = fArr[4];
		boolean cosec = fArr[5];
		
	
		Thread th;
			
		InputFix.fixDegrees(textDegrees, controller);
		InputFix.fixIterations(textIter, controller);
		InputFix.fixPrecision(textPrec, controller);
			
		String newDegrees = controller.getInputDegrees();
		String newIters = controller.getInputIterations();
		String newPrec = controller.getInputPrecision();
		
		String outnotcalctext = Language.getTranslateLines().get("notcalc");
		String outtext = Language.getTranslateLines().get("wait");

		controller.setTextOutput(outtext);
			
		if(sin && !(cos && tg && ctg && sec && cosec))
		{
			th = new Thread(() -> runCalcSin(newDegrees, Integer.valueOf(newIters), Integer.valueOf(newPrec)));
			th.setDaemon(true);
			th.start();
		}
		else if (cos && !(sin && tg && ctg && sec && cosec))
		{
			th = new Thread(() -> runCalcCos(newDegrees, Integer.valueOf(newIters), Integer.valueOf(newPrec)));
			th.setDaemon(true);
			th.start();
		}
			
		else if (tg && !(sin && cos && ctg && sec && cosec))
		{
			//double checkDegrees = Double.parseDouble(textDegrees);
			if (new BigDecimal(newDegrees).compareTo(new BigDecimal("90")) == 0 || new BigDecimal(newDegrees).compareTo(new BigDecimal("270")) == 0)
			{
				controller.setTextOutput(outnotcalctext);
				controller.setButtonStartEnable(true);
			}
			else
			{
				th = new Thread(() -> runCalcTg(newDegrees, Integer.valueOf(newIters), Integer.valueOf(newPrec)));
				th.setDaemon(true);
				th.start();
			}
		}
		else if (ctg && !(sin && cos && tg && sec && cosec))
		{
			//double checkDegrees = Double.parseDouble(textDegrees);
			if (new BigDecimal(newDegrees).compareTo(new BigDecimal("0")) == 0 || new BigDecimal(newDegrees).compareTo(new BigDecimal("180")) == 0)
			{
				controller.setTextOutput(outnotcalctext);
				controller.setButtonStartEnable(true);
			}
			else
			{
				th = new Thread(() -> runCalcCtg(newDegrees, Integer.valueOf(newIters), Integer.valueOf(newPrec)));
				th.setDaemon(true);
				th.start();
			}
		}
		else if (sec && !(sin && cos && tg && ctg && cosec))
		{
				
			if (new BigDecimal(newDegrees).compareTo(new BigDecimal("90")) == 0 || new BigDecimal(newDegrees).compareTo(new BigDecimal("270")) == 0)
			{
				controller.setTextOutput(outnotcalctext);
				controller.setButtonStartEnable(true);
			}
			else
			{
				th = new Thread(() -> runCalcSec(newDegrees, Integer.valueOf(newIters), Integer.valueOf(newPrec)));
				th.setDaemon(true);
				th.start();
			}
		}
		else if (cosec && !(sin && cos && tg && sec && ctg))
		{
			
		if (new BigDecimal(newDegrees).compareTo(new BigDecimal("0")) == 0 || new BigDecimal(newDegrees).compareTo(new BigDecimal("180")) == 0)
		{
			controller.setTextOutput(outnotcalctext);
			controller.setButtonStartEnable(true);
		}
		else
		{
			th = new Thread(() -> runCalcCosec(newDegrees, Integer.valueOf(newIters), Integer.valueOf(newPrec)));
			th.setDaemon(true);
			th.start();
			}
		}
				
		else
		{
			System.out.println("More than one function is selected");
			controller.setButtonStartEnable(true);
		}
	}
	
	public void runCalcSin(String tD, int tI, int tP)
	{
		
		String outtext = CalcTrigonometricValue.calcSin(tD, tI, tP, Settings.getForceIterations(), controller);
		preStartAction(outtext);
	}
	
	public void runCalcCos(String tD, int tI, int tP)
	{
		String outtext = CalcTrigonometricValue.calcCos(tD, tI, tP, Settings.getForceIterations(), controller);
		preStartAction(outtext);
	}
	
	public void runCalcTg(String tD, int tI, int tP)
	{
		String outtext = CalcTrigonometricValue.calcTg(tD, tI, tP, Settings.getForceIterations(), controller);
		preStartAction(outtext);
	}
	
	public void runCalcCtg(String tD, int tI, int tP)
	{
		String outtext = CalcTrigonometricValue.calcCtg(tD, tI, tP, Settings.getForceIterations(), controller);
		preStartAction(outtext);
	}
	
	public void runCalcSec(String tD, int tI, int tP)
	{
		String outtext = CalcTrigonometricValue.calcSec(tD, tI, tP, Settings.getForceIterations(), controller);
		preStartAction(outtext);
	}
	
	public void runCalcCosec(String tD, int tI, int tP)
	{
		String outtext = CalcTrigonometricValue.calcCosec(tD, tI, tP, Settings.getForceIterations(), controller);
		preStartAction(outtext);
	}

	
	private void preStartAction(String outtext)
	{
		controller.setTextOutput(outtext);
		controller.setButtonStartEnable(true);
		controller.setButtonSaveEnable(true);
		controller.setButtonCopyEnable(true);
	}
}
