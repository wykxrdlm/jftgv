package net.wykxrdlm.jftgv;

//toggles all other radio buttons if interacting 
public class RadioSwitcher
{
	public static void enableRadioSin(InterfaceController controller)
	{
		controller.setBoolRadioSin(true);
		controller.setBoolRadioCos(false);
		controller.setBoolRadioTg(false);
		controller.setBoolRadioCtg(false);
		controller.setBoolRadioSec(false);
		controller.setBoolRadioCosec(false);
		
	}
	
	public static void enableRadioCos(InterfaceController controller)
	{
		controller.setBoolRadioSin(false);
		controller.setBoolRadioCos(true);
		controller.setBoolRadioTg(false);
		controller.setBoolRadioCtg(false);
		controller.setBoolRadioSec(false);
		controller.setBoolRadioCosec(false);
	}
	
	public static void enableRadioTg(InterfaceController controller)
	{
		controller.setBoolRadioSin(false);
		controller.setBoolRadioCos(false);
		controller.setBoolRadioTg(true);
		controller.setBoolRadioCtg(false);
		controller.setBoolRadioSec(false);
		controller.setBoolRadioCosec(false);
	}
	
	public static void enableRadioCtg(InterfaceController controller)
	{
		controller.setBoolRadioSin(false);
		controller.setBoolRadioCos(false);
		controller.setBoolRadioTg(false);
		controller.setBoolRadioCtg(true);
		controller.setBoolRadioSec(false);
		controller.setBoolRadioCosec(false);
	}
	
	public static void enableRadioSec(InterfaceController controller)
	{
		controller.setBoolRadioSin(false);
		controller.setBoolRadioCos(false);
		controller.setBoolRadioTg(false);
		controller.setBoolRadioCtg(false);
		controller.setBoolRadioSec(true);
		controller.setBoolRadioCosec(false);
	}
	
	public static void enableRadioCosec(InterfaceController controller)
	{
		controller.setBoolRadioSin(false);
		controller.setBoolRadioCos(false);
		controller.setBoolRadioTg(false);
		controller.setBoolRadioCtg(false);
		controller.setBoolRadioSec(false);
		controller.setBoolRadioCosec(true);
	}
}
