# What is jFTGV?

The program allows you to calculate the basic \(not inverse\) trigonometric functions with high accuracy _\(with standard settings up to ~2^16 decimal places\)_. The mathematical basis of the program is the Taylor Series, which information about can be found [here](https://en.wikipedia.org/wiki/Taylor_series) or on more specialized websites. The technical base of the program is the JavaFX library, which is a standard Java library on version 1.8. The jFTGV is complete and will no longer be updated and supported. You can use jFTGV for proprietary purposes, I will be glad if you let me know about it.

# How to use the program?

When you start the program, you see several lines to enter. In the first input line, you must specify the degree whose value of the trigonometric function you want to calculate. In the second input line of input, you need to specify the number of iterations. The more iterations, the higher the accuracy of the final result! Sometimes you may not need it, so you can specify a small iterations amount in this input line. In the third line of input, you need to specify how many decimal places you want to see in the final result. Next, select the trigonometric function, the value of the degree that you want to calculate. After that, start the program by clicking the Start button!

  
It is important to understand that the number of iterations and the number of decimal places directly affects the speed of calculations! You can see at what stage the calculations are at near the output window. By the way, most often, most iterations will be redundant for you. In order not to waste computer resources, you can change the configuration file, which will be described later.

# Is there something else?

Yes, of course. jFTGV has a number of different things that you can change. These are such things as the configuration, the language and the theme. Through this, you can modify the program.

### Configuration

The main configuration file looks like this. It also explains what this or that setting does.

‌

```text
file_translate=lang_en <- localization file name 
file_theme=dark <- main FXML file name
app_width=640 <- app window width 
app_height=480 <- app window height
app_resizeable=false <- if true app is resizeable
logic_force_iterations=true <- if true the program will only calculate until the final result
limits_iterations_min=4 <- minimum number of iterations. must be more than one
limits_iterations_max=8191 <- maximum number of iterations
limits_precision_min=2 <- minimum number of digits after decimal point. must be more than one
limits_precision_max=65535 <- maximum number of digits after decimal point
```

### Language

The translation file looks about the same as the main configuration file. A list of all languages will be provided at the end of the README. You can find all localization files in the languages folder and create a new one.

‌

```text
info_creator_text=creator 
info_version_text=version 
info_translate_text=english translation
info_wait=wait...
info_not_calc=cannot be calculated
label_degrees_text=select degree
label_iterations_text=number of iterations
label_precision_text=number of digits after the decimal dot
label_function_text=select trigonometric function
label_start_text=get value
label_output_text=output
button_start_text=start
button_copy_text=copy
button_save_text=save...
```

### Theme

I call the "theme" the main FXML file that JavaFX can use. You can create a "themes" file either manually or using the Scene Builder. There are mandatory components and functions that you must use, you can get acquainted with them by looking at the InterfaceController class. There are two standard themes, light and dark.

# How does the program work?

It all starts with the _FindTrigonometricValue_ class. This is the first class that starts working in the program. An FXML file \(_Theme_ class\) and a configuration file \(_Settings_ class\) are loaded in it, the main window is displayed and everything is prepared for the program to work. _InterfaceController_ is a controller class that is set using an FXML file. With the help of _InputListeners_, we restrict the input of unwanted characters where we need it. If you interact with the program in any way, then you interact with the _InterfaceHandler_ class, which mainly processes buttons and prepares the program for starting calculations, _RadioSwitcher_, which is responsible for turning off radio buttons. When starting calculations, _InputFix_ is called. This is a class that restricts the numbers you entered and brings them to uniformity. After that, the calculations themselves are performed in the _CalcTrigonometricValue_ class. _CalcTrigonometricValue_ uses the number Pi, which is set in the _PiValue_ class. All program constants are set in _ProgramConstants_. The file paths are mainly indicated there.

You can use jFTGV by loading it as an external JAR! Import the _CalcTrigonometricValue_ class and use one of the functions for your own purposes. It looks something like this:

‌

```java
package ...

import net.wykxrdlm.jftgv.CalcTrigonometricValue;

public class TestFTGV
{
    public static void main (String args[])
	{
         System.out.println( CalcTrigonometricValue.calcCos( String.valueOf(30), 120, 48, true ) );
    }
}
```

This code calculates the cosine of 30 degrees, using 120 iterations to get a result with 24 digits after the decimal point and forcing the program to continue iterating.

# Languages that jFTGV has been translated into:


* English
* Russian
* German

# Download


You can download the program [here](https://pastebin.com/JH1xhmud).